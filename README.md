*** Ready to use environment for Bitrix CMS based on https://docs.docker.com/compose/.

* Features:

1. apache2 with php5.6 with doc_root mounted to ./www directory on host system. 
2. mysql5.6 with db in peristent volume on host system.
3. System configured with all bitrix requirments. No errors or warnings on CMS selftest page http://your-host.example/bitrix/admin/site_checker.php
4. Preinstalled http://your-host.example/restore.php allow simple restore bitrix maked backup.
5. Preinstalled ssmtp agent for configuring outgoing mail via external service.

* Installing:

Clone this repo.
Edit docker-compose.yml corresponding to your requirments.
Copy and edit with your credentials ssmtp/ssmtp.conf.example to ssmtp/ssmtp.conf
Change directory to root of this project (same directory where docker-compose.yml placed)
Run docker-compose up
Go to http://localhost/restore.php or http://your-host.example/restore.php or http://ip-of-your-host/restore.php
restore existing backup of bitrix site. Or just point in docker-compose.yml ./www to directory with existing installation.
You also can replace mysql service with external mysql service for connecting to existing mysql db. (example included in docker-compose.yml.external)

*** Готовое к использованию преднастроенное окружение для Bitrix CMS на основе https://docs.docker.com/compose/.

* Особенности:

1. apache2 c php5.6 c dock_root смонтированным в локальную директорию ./www на хост системе.
2. mysql5.6 с БД хранящейся в постоянном томе docker.
3. Система настроена с учетом всех требований Bitrix CMS. Из коробки нет ошибок на странице самотестирования Bitrix http://your-host.example/bitrix/admin/site_checker.php
4. Предустановленный скрипт http://your-host.example/restore.php позволяет просто развернуть стандартный бекап Bitrix.
5. Предустановленный в контейнер с apache - ssmtp. Позволяет настроить исходящую почту через внешнюю службу.

* Установка:

Загрузите этот репозиторий.
Отредактируйте docker-compose.yml в соотвествии с вашими требованиями.
Скопируйте ssmtp/ssmtp.conf.example в ssmtp/ssmtp.conf и отредактируйте в соотвествии с настройками вашего SMTP сервера.
Перейдите в корневую директорию этого проекта и выполните docker-compose up
В браузере перейдите по адресу http://localhost/restore.php or http://your-host.example/restore.php or http://ip-of-your-host/restore.php в зависимости от того где именно вы развернули окружение.
Так же вы можете изменить ./www в docker-compose.yml на директорию с существующей инсталляцией bitrix и изменить службу mysql на внешнюю для использования существующей БД.
